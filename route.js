/* CREATE A ROUTE
"/greeting"
*/
const http = require("http");
const port = 4000;

let server = http.createServer((request, response) => {
  if (request.url == "/greeting") {
    response.writeHead(200, { "Content-Type": "text/plain" });
    response.end("Hello Route!");

    // Homepage route
  } else if (request.url == "/homepage") {
    response.writeHead(200, { "Content-Type": "text/plain" });
    response.end("Hello Home!");
  } else {
    response.writeHead(400, { "Content-Type": "text/plain" });
    response.end("Page not Available!");
  }
});
server.listen(4000);

console.log(`Server running at localhost:${port}`);
