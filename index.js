// require directive
// module - sowftware component contains one ore more routines
// http module transfer data using hypertest transfer protocol
// clients(browers) and servers(node.js/express.js app)

// request - messages sent by client
// responses - messages sent by server as an answer to client

let http = require("http");

// create a server
// createServer()

http
  .createServer((request, response) => {
    response.writeHead(200, { "Content-Type": "text/plain" });
    response.end("Hello World!");
  })
  .listen(4000);

console.log("Server running at localhost:4000");
// port is virtual point for start and end network connection

// writeHead()
// set a status code for response - a 200 - OK

// node filename.js to run
// npm install -g nodemon
// nodemon-v
